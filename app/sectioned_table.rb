module IWSectionedStaticTableDelegates
  attr_accessor :sections

  CELL_CONTENT_WIDTH = 320.0
  CELL_CONTENT_MARGIN = 10.0

  # def init
  #   if self.initWithStyle UITableViewStyleGrouped
  #     @sections = []
  #     # self.dataSource = self
  #     # self.delegate = self
  #   end
  #   self
  # end

  def clear_table
    @sections = []
  end

  def add_section(header = nil, footer = nil, &blk)
    @sections ||= []
    section = IWSection.new(header, footer)
    @sections << section
    blk.call(section) if blk
  end

# datasource

  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell_config = @sections[indexPath.section].cells[indexPath.row]
    cell = cell_config.klass.alloc.initWithStyle(cell_config.style, reuseIdentifier:cell_config.reuse)
    # puts "going to configure #{cell}"
    cell_config.configure_block.call(cell) if cell_config.configure_block
    return cell
  end

  def tableView(tableView, numberOfRowsInSection:section)
    #puts "rows in section #{ @sections[section].cells.size}"
    @sections[section].cells.size
  end

  def numberOfSectionsInTableView(tableView)
    # puts "section size #{@sections.size}"
    @sections ? @sections.size : 0
  end

  def tableView(tableView, titleForHeaderInSection:section)
    @sections[section].header_title
  end

  def tableView(tableView, titleForFooterInSection:section)
    #Log.debug("setting footer to #{@sections[section].footer_title} for #{object_id}")
    @sections[section].footer_title

  end

# delegate
  def tableView(tableView, heightForRowAtIndexPath:indexPath)
    # puts "HEIGHT = #{@sections[indexPath.section].cells[indexPath.row].height}"
    cell_config =  @sections[indexPath.section].cells[indexPath.row]
    #puts "height for row is #{cell_config.height}"
    return cell_config.height

  end

  def tableView(tableView, willSelectRowAtIndexPath:indexPath)
    cell_config = @sections[indexPath.section].cells[indexPath.row]
    if cell_config.selected_block
      cell_config.selected_block.call
    end
    return nil
  end

  # convenience
  def cell_height_for_text(text, font)
    constraint = CGSizeMake(CELL_CONTENT_WIDTH - (CELL_CONTENT_MARGIN * 2), 20000.0)
    size = text.sizeWithFont(font, constrainedToSize:constraint, lineBreakMode:UILineBreakModeWordWrap)
    height = [size.height, 44.0].max
    #puts "calculated height #{height + (CELL_CONTENT_MARGIN * 2)}"
    return height + (CELL_CONTENT_MARGIN * 2)

  end
end

class IWSection
  attr_accessor :cells, :header_title, :footer_title
  def initialize(header_title = nil, footer_title = nil)
    @header_title = header_title
    @footer_title = footer_title
    @cells = []
  end

  def add_cell(klass, style, height, configblock, selectedblock)
    # puts "add_cell height #{height}"
    cell = IWUITableViewCellConfig.new(style, height)
    cell.configure_block = configblock
    cell.selected_block = selectedblock
    cell.klass = klass
    @cells << cell
  end

  def dealloc
    puts("deallocing iwsection #{self}")
    super
  end
end


class IWUITableViewCellConfig
  attr_accessor :selected_block, :style, :configure_block, :reuse, :height, :klass

  def initialize(style, height)
    @height = height || UITableViewAutomaticDimension
    # puts "SET HEIGHT to #{height} #{@height}"
    @style = style || UITableViewCellStyleDefault
    @reuse = "DefaultCell"
  end

  def dealloc
    puts("deallocing iwcellconfig #{self}")
    super
  end
end
