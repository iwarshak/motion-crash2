class Spot
  @@att = [:id, :name, :description, :lat, :lng, :altitude, :altitude_accuracy, :location_accuracy,
           :location_timestamp, :edit_url, :delete_url, :detail_url, :contacts_count, :default_spot_color, :category_id,
           :reminder_count]
  attr_accessor *@@att




  def init_from_json(json)
    @@att.each do |a|
      self.send("#{a}=", json[a.to_s])
    end

    if cat=json['category']
      self.category = Category.alloc.init_from_json(cat)
    end



    return self
  end






end
