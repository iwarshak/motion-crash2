class MyTable < UIViewController
  attr_accessor :spot, :table
 include IWSectionedStaticTableDelegates

  def dealloc
    puts("deallocing table conteoller!!!!!!!!!!!")
    super
  end

  def viewDidDisappear(a)
    Log.debug("disa[pearing. count is #{self.retainCount}")
    super
  end

  def initWithSpot(spot)
    self.spot = spot
    #setup_observers
    return init
  end

  def loadView
    @table = UITableView.alloc.initWithFrame([[0,0],[0,0]], style:UITableViewStyleGrouped)
    @table.dataSource = self
    @table.delegate = self
    self.view = @table
    setup_table
  end


  def setup_table
    clear_table
    add_section('Actions', nil) do |section|
      cfg = nil
      cfg = lambda{|cell|} ## this is  cycle that should be detected
      section.add_cell(UITableViewCell, UITableViewCellStyleDefault, nil, cfg,nil)
    end
    @table.reloadData


  end


end
