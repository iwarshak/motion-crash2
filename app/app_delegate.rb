class Log
  def self.debug(s)
    puts s
  end
  def self.info(s)
    puts s
  end
  def self.warn(s)
    puts s
  end

end


class AppDelegate
  def application(application, didFinishLaunchingWithOptions:launchOptions)
    @window = UIWindow.alloc.initWithFrame(UIScreen.mainScreen.bounds)
    myNavController = RootController.alloc.init

    @window.rootViewController = UINavigationController.alloc.initWithRootViewController(myNavController)

    @window.makeKeyAndVisible
    true
  end
end
