class RootController < UIViewController
  def viewDidLoad
    @spot = Spot.new
    @spot.name = 'foo'
    but = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    but.setTitle('works', forState:UIControlStateNormal)
    but.frame = [[100,100], [50,50]]
    $b = but
    but.addTarget(self, action:'works', forControlEvents:UIControlEventTouchUpInside)
    self.view.addSubview(but)


    but = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    but.setTitle('doesnt', forState:UIControlStateNormal)
    but.frame = [[200,200], [50,50]]
    but.addTarget(self, action:'doesnt', forControlEvents:UIControlEventTouchUpInside)
    self.view.addSubview(but)

    but = UIButton.buttonWithType(UIButtonTypeRoundedRect)
    but.setTitle('plaintable', forState:UIControlStateNormal)
    but.frame = [[50,200], [150,50]]
    but.addTarget(self, action:'sorta', forControlEvents:UIControlEventTouchUpInside)
    self.view.addSubview(but)
  end

  def viewWillAppear(a)
    super
  end

  def viewDidDisappear(a)
    # $a = self
    # puts "disappear #{self} retain #{self.retainCount}"
    super
  end


  def works()
    s = Spot.new
    s.name = 'go back, and this vc will dealloc'
    navigationController.pushViewController(MyTable.alloc.init, animated:true)
    # App.alert('boom')
  end

  def sorta()
    s = Spot.new
    s.name = 'go back, and this vc will dealloc'
    navigationController.pushViewController(PlainTable.alloc.init, animated:true)
    # App.alert('boom')
  end


  def doesnt
    navigationController.pushViewController(SpotsListController.alloc.init, animated:true)
  end


  def dealloc
    puts "deallocing #{self}"
  end

end
