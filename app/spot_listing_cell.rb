class SpotListingCell < UITableViewCell
  extend UITableViewCellHelper

  attr_accessor :spot
  Identifier = 'spot-listing-cell'

  def initWithStyle(style, reuseIdentifier:identifier)
    #Log.debug("initting spotlistingcell #{style} subtitle is #{UITableViewCellStyleSubtitle}")
    super(UITableViewCellStyleSubtitle, reuseIdentifier:identifier)
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator
    self
  end


  def object=(o)
    self.spot = o
  end
  def object
    self.spot
  end


  def spot=(s)
    @spot = s
    if s
      self.textLabel.text = s.name

    end
  end



  def self.height(spot)
    font = UIFont.systemFontOfSize(18.0)

    detail_font = UIFont.systemFontOfSize(14.0)
    #font = UIFont.fontWithName("OpenSans", size:14.0);
    text = spot.name

    #detail_font = UIFont.fontWithName("OpenSans", size:12.0);

    return 60.0
  end


end
