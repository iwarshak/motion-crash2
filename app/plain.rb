class PlainTable < UITableViewController

  def init
    initWithStyle(UITableViewStylePlain)
    self.tableView.delegate = self
    self.tableView.dataSource = self
  end


  def tableView(tableView, numberOfRowsInSection:section)
    1
  end


  def tableView(tableView, cellForRowAtIndexPath:indexPath)
    cell = tableView.dequeueReusableCellWithIdentifier('fff')
    unless cell
      cell = UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:'fff')
    end
    cell.textLabel.text = 'click me'

    return cell
  end


  def tableView(tableView, didSelectRowAtIndexPath:indexPath)
        detail_view = MyTable.alloc.init
    self.navigationController.pushViewController(detail_view, animated:true)


  end

  def dealloc
    Log.debug('deallocing plain table controller')
    super
  end




end
