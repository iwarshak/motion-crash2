class SearchablePageableController < UITableViewController
  LoadingCellTag = 990

  attr_accessor :all_total_pages, :all_current_page, :objects, :filtered_objects, :filtered_current_page,
                :filtered_total_pages, :current_search, :cell_class, :refresh_control, :load_objects_proc, :search_proc,
                :cell_config_proc, :hide_search_bar, :last_loaded_at, :reload_every

  def init
    reset_data
    self.hide_search_bar=true
    super

  end

  def reset_data
    self.all_total_pages = 0
    self.all_current_page = 0
    self.filtered_total_pages = 0
    self.filtered_current_page = 0
    self.objects=[]
    self.filtered_objects=[]
  end

  def reload_objects
    Log.debug("reload_objects #{self}")
    reset_data
    self.all_current_page=1
    self.load_objects

  end

  def load_objects
    #Log.debug "\n\n\n\n loading page #{all_current_page} \n\n\n\n"
    load_objects_proc.call(all_current_page, lambda{ |opts|
      #Log.debug("in LOAD OBHJEC")
      #Log.debug("in LOAD #{self}")
      self.last_loaded_at=Time.now
      self.all_total_pages = opts['total_pages']
      self.objects = [] if @refreshing

      self.objects += opts['objects']
      self.tableView.reloadData
      @refresh_control.endRefreshing if @refresh_control
      @refreshing = false
      Dispatch::Queue.main.async {self.tableView.reloadData}


    }, lambda{ |error|
      Log.warn "got error in load_objects #{error} in #{self}"

      @refresh_control.endRefreshing if @refresh_control
      @refreshing=false
    }) if load_objects_proc


  end


  def search_for(searchString)
    self.current_search=searchString
    search_proc.call(searchString, filtered_current_page, lambda{|opts|
      if opts['search_string'] != searchString || searchString != current_search
        Log.debug("got an old search result json: #{opts['search_string']} searchString #{searchString} current_search:#{current_search}")
      else
        self.filtered_total_pages=opts['total_pages']
        @filtered_objects += opts['objects']
        Log.debug "found #{@filtered_objects.size} activities. pages #{filtered_total_pages} in #{self}"
        @search_controller.searchResultsTableView.reloadData
      end
      }, lambda{|error|
          Log.warn "got error search_for #{error} in #{self}"
    })
  end


  def searchDisplayController(controller, shouldReloadTableForSearchString:searchString)
    #Log.debug "resetting search results. current search #{searchString}"
    self.filtered_objects = []
    self.filtered_total_pages=0
    self.filtered_current_page=1
    self.current_search=searchString

    search_for(searchString) unless searchString.blank?
    return false
  end

  def  searchDisplayControllerWillEndSearch(controller)
    puts "ending search"
    @filtered_objects = []
    @all_current_page = 1
  end

  def loading_cell
    cell = UITableViewCell.alloc.initWithStyle(UITableViewCellStyleDefault, reuseIdentifier:nil)
    activity = UIActivityIndicatorView.alloc.initWithActivityIndicatorStyle(UIActivityIndicatorViewStyleGray)
    activity.center = cell.center
    cell.addSubview(activity)
    activity.startAnimating
    cell.tag = LoadingCellTag
    return cell
  end


  def viewDidLoad
    setup_search_controller
    add_search_button

    @refresh_control = UIRefreshControl.alloc.init
    self.refreshControl = @refresh_control
    # @refresh_control.when(UIControlEventValueChanged) do
    #   #Log.debug("refreshing")
    #   @refreshing = true
    #   self.all_current_page=1
    #   self.load_objects
    # end
    super

  end

  def viewWillAppear(a)
    super
    Log.debug("objects last_loaded_at  #{last_loaded_at} reload_every #{reload_every} Time.now #{Time.now} #{self}")

    # if last_loaded_at && reload_every && ((Time.now - last_loaded_at) >= reload_every.to_f)
    #   Log.debug("data is outdataed. reloading #{self}")
    #   Dispatch::Queue.main.async {
    #     @refresh_control.beginRefreshing if @refresh_control
    #     self.view.setContentOffset([0,-44], animated:false)  # show the table to the top enough to show the reload control
    #   }
    #   reload_objects
    # end
  end



  # DELEGATE/datasrc

  def tableView(tv, willSelectRowAtIndexPath:indexPath)
    #if tv == tableView
    #
    #end

    cell = tableView(tv, cellForRowAtIndexPath:indexPath)
    if cell.tag == LoadingCellTag
      return nil
    else
      return indexPath
    end
  end

  def tableView(tv, cellForRowAtIndexPath:indexPath)
    if tv == tableView
      #Log.debug "looking for row #{indexPath.row} src is objects #{objects.size}"
      src = objects
    else
      #Log.debug "looking for row #{indexPath.row} src is filtered_objects #{filtered_objects.size}"
      src = filtered_objects
    end

    if indexPath.row < src.count
      if defined?(cell_class::Identifier)
        ident = cell_class::Identifier
      else
        ident = cell_class.to_s
      end
      cell = tv.dequeueReusableCellWithIdentifier(ident) || cell_class.alloc.init
      #Log.debug "getting activity #{indexPath.row} of #{src.size} its #{src[indexPath.row]} "
      cell.object = src[indexPath.row]
      if cell_config_proc
        cell.instance_eval &cell_config_proc
      end
    else
      #Log.debug "showing loading cell"
      cell = loading_cell
    end

    return cell
  end

  def tableView(tv, numberOfRowsInSection:section)
    if tv == tableView
      counts = [all_current_page, all_total_pages]
      src = objects
    else

      counts = [filtered_current_page, filtered_total_pages]
      #Log.debug("filtered counts #{counts}")
      if counts[0] == 0 && counts[1] == 0
        return 0
      end
      src = filtered_objects
    end
    #Log.debug("numberofrows counts: #{counts}")

    if counts[0] == 0
      #Log.debug "numberofrows: returning 1 row for spinner"
      return 1 #activity cell
    end
    if counts[0] < counts[1]
      #Log.debug "numberofrows: returning #{src.size} plus 1 for spinner"
      return src.size + 1 # one for spinner
    end


    #Log.debug "numberofrows: returning #{src.size} rows"
    return src.size

  end



  def tableView(tv, willDisplayCell:cell, forRowAtIndexPath:indexPath)
    if tv == tableView
      if cell.tag == LoadingCellTag && !refresh_control.refreshing?
        #Log.debug "current page #{all_current_page}. loading spots"
        self.all_current_page += 1
        self.load_objects
      end
    else
      if cell.tag == LoadingCellTag
        #Log.debug "current page #{filtered_current_page}. loading spots"
        self.filtered_current_page += 1
        self.search_for(current_search)
      end
    end

  end

  def tableView(tableView, didSelectRowAtIndexPath:indexPath)
    if tableView == self.tableView
      src = objects
    else
      src = filtered_objects
    end
    row_selected src[indexPath.row]


  end

  def row_selected(object)
    # to be overridden
  end

  def tableView(tv, heightForRowAtIndexPath:indexPath)
    if tv == tableView
      src = objects
    else
      src = filtered_objects
    end
    if src.empty? || indexPath.row >= src.size
      return 44  # spinner
    end
    if cell_class && cell_class.respond_to?(:height)
      cell_class.height(src[indexPath.row])
    else
      44
    end
  end


  # setup

  def setup_search_controller
    @search_bar = UISearchBar.alloc.initWithFrame(CGRectMake(0,0,tableView.frame.size.width, 44))
    @search_controller = UISearchDisplayController.alloc.initWithSearchBar(@search_bar, contentsController: self)
    @search_controller.delegate = self  # tried WeakRef.new(self), but didn't work
    @search_controller.searchResultsDataSource = self
    @search_controller.searchResultsDelegate = self

    self.tableView.tableHeaderView = @search_bar

    if hide_search_bar
      # // Hide the search bar until user scrolls up
      newBounds = self.tableView.bounds
      newBounds.origin.y = newBounds.origin.y + @search_bar.bounds.size.height
      self.tableView.bounds = newBounds
    end
  end

  def add_search_button
    rightButton = UIBarButtonItem.alloc.initWithBarButtonSystemItem(UIBarButtonSystemItemSearch, target:self,
      action:'show_search')
    self.navigationItem.rightBarButtonItem = rightButton
  end

  def show_search
    @search_bar.becomeFirstResponder
  end


  def new_object(o)
    if objects.any? # if we just push one onto an empty array, it won't load in the rest automatically
      Log.debug("added #{o} to objects and reloadData #{self}")
      objects.unshift(o)
      view.reloadData
    else
      Log.debug("no objects in #{self} so reloading_objects")
      reload_objects
    end
  end


  def object_updated(o)
    Log.debug("object_updated #{o} #{self}")
    objects.map!{|object| object.id == o.id ? o : object}

    filtered_objects.map!{|object| object.id == o.id ? o : object}

    @search_controller.searchResultsTableView.reloadData if @search_controller && @search_controller.searchResultsTableView

    #Log.debug "DONE upaing object"
    #if filtered_objects.select{|object| object.id == o.id}.any?
    #  filtered_objects.map!{|object| object.id == o.id ? o : object}
    #  Log.debug("SEATXHCON is #{@search_controller} id:#{self.id}")
    #  Log.debug("SEATXHCON VIEW is #{@search_controller.searchResultsTableView}")
    #end


    view.reloadData
  end


  def object_deleted(o)
    #Log.info("deleted object #{notification.object}")
    #Log.debug("filtered objects #{filtered_objects.collect{|c|c.name}}")
    filtered_reject = []
    filtered_objects.delete_if {|c| filtered_reject << c if c.id == o.id }
    if filtered_reject.any?
      Log.debug("reloadData filteredobjects #{self} resiltstableview #{@search_controller.searchResultsTableView}")
      @search_controller.searchResultsTableView.reloadData if @search_controller && @search_controller.searchResultsTableView
    end
    Log.debug("current_objects #{objects.collect{|c|c.id}.join(', ')} deleted_id #{o.id} ")
    reject = []
    objects.delete_if {|c| reject << c if c.id == o.id }
    if reject.any?
      Log.debug("reloadData #{self}")
      view.reloadData
    end

  end


  def dealloc
    Log.debug("deallocing #{self} id #{self.object_id}")

    @search_controller.delegate=nil
    @search_controller.searchResultsDataSource = nil
    @search_controller.searchResultsDelegate = nil
    super
  end

end
