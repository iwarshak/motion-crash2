module UITableViewCellHelper
  def height_for_text(pairs = {})

    cell_content_width = 280.0
    cell_content_margin = 10.0

    constraint = CGSizeMake(cell_content_width - (cell_content_margin * 2), 20000.0)
    total = 0.0

    pairs.each do |text, font|
      next unless text
      size = text.sizeWithFont(font, constrainedToSize:constraint, lineBreakMode:UILineBreakModeWordWrap)
      #Log.debug("height #{size.height} for text #{text}")
      total += size.height
    end
    height = [total].max + (cell_content_margin * 2)
    #Log.debug "returning height of #{height} for #{pairs.keys.join('-')}"
    return height

  end
end
